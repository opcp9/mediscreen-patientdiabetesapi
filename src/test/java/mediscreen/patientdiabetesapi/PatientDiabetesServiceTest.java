package mediscreen.patientdiabetesapi;

import mediscreen.patientdiabetesapi.dal.client.PatientApiClient;
import mediscreen.patientdiabetesapi.dal.client.PatientNoteApiClient;
import mediscreen.patientdiabetesapi.domain.model.Patient;
import mediscreen.patientdiabetesapi.domain.model.PatientDiabetesReport;
import mediscreen.patientdiabetesapi.domain.model.PatientNote;
import mediscreen.patientdiabetesapi.domain.service.DateHelper;
import mediscreen.patientdiabetesapi.domain.service.PatientDiabetesService;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.util.Collections;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
class PatientDiabetesServiceTest {

    @InjectMocks
    private PatientDiabetesService patientDiabetesService;

    @Mock
    private PatientApiClient patientApiClient;
    @Mock
    private PatientNoteApiClient patientNoteApiClient;
    @Mock
    private DateHelper dateHelper;

    @Test
    void should_returnEarly_when30m5() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2030, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("M"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin Poids anOrmal Cholestérol";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("Early Onset");
    }

    @Test
    void should_returnEarly_when30f7() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2030, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("F"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin Poids anOrmal Cholestérol Vertige Antibodies";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("Early Onset");
    }

    @Test
    void should_returnEarly_when50m8() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2050, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("M"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin Poids anOrmal Cholestérol Vertige Antibodies Reaction";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("Early Onset");
    }

    @Test
    void should_returnDanger_when30m3() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2030, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("M"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin Poids";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("In Danger");
    }

    @Test
    void should_returnDanger_when30f4() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2030, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("F"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin Poids Antibodies";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("In Danger");
    }

    @Test
    void should_returnDanger_when50f6() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2030, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("F"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin Poids anOrmal Cholestérol Vertige";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("In Danger");
    }

    @Test
    void should_returnBorderline_when50f2() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2050, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("F"));
        String fakePatientNotes = "Hémoglobine A1C Microalbumin";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("Borderline");
    }

    @Test
    void should_returnBorderline_when30m0() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2030, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("M"));
        String fakePatientNotes = "";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("None");
    }

    @Test
    void should_returnBorderline_when50f0() {
        when(dateHelper.now()).thenReturn(LocalDate.of(2050, 1, 2));
        when(patientApiClient.getPatientById(anyInt())).thenReturn(getFakePatient("F"));
        String fakePatientNotes = "";
        when(patientNoteApiClient.getPatientNotesByPatientId(anyInt())).thenReturn(Collections.singletonList(getFakePatientNote(fakePatientNotes)));

        PatientDiabetesReport patientDiabetesReport = patientDiabetesService.getPatientDiabetesReportByPatientId(1);

        assertThat(patientDiabetesReport.getPatientDiabetesRating()).isEqualTo("None");
    }

    private PatientNote getFakePatientNote(String fakePatientNotes) {
        PatientNote patientNote = new PatientNote();
        patientNote.setId("1");
        patientNote.setPatientId(1);
        patientNote.setPatientNote(fakePatientNotes);
        return patientNote;
    }

    private Patient getFakePatient(String sex) {
        Patient patient = new Patient();
        patient.setId(1);
        patient.setFamily("FamilyTest");
        patient.setGiven("GivenTest");
        patient.setDob(LocalDate.of(2000, 1, 1));
        patient.setSex(sex);
        patient.setAddress("AddressTest");
        patient.setPhone("PhoneTest");
        return patient;
    }
}
