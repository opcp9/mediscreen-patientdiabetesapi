package mediscreen.patientdiabetesapi;

import feign.FeignException;
import mediscreen.patientdiabetesapi.domain.model.Patient;
import mediscreen.patientdiabetesapi.domain.model.PatientDiabetesRating;
import mediscreen.patientdiabetesapi.domain.model.PatientDiabetesReport;
import mediscreen.patientdiabetesapi.domain.service.PatientDiabetesService;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;

import java.time.LocalDate;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest
public class PatientDiabetesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PatientDiabetesService patientDiabetesService;

    @Test
    public void should_returnPatientDiabetesReport_whengetPatientDiabetesReportByExistingPatientId() throws Exception {
        when(patientDiabetesService.getPatientDiabetesReportByPatientId(anyInt())).thenReturn(getFakePatientDiabetesReport());

        mockMvc.perform(get("/patientdiabetes?patientId=1"))
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(String.valueOf(getFakePatientDiabetesReport().getPatientDiabetesRating()))));
    }

    @Test
    public void should_returnNotFound_whengetPatientDiabetesReportByMissingPatientId() throws Exception {
        when(patientDiabetesService.getPatientDiabetesReportByPatientId(anyInt())).thenThrow(FeignException.class);

        mockMvc.perform(get("/patientdiabetes?patientId=1"))
                .andExpect(status().isNotFound());
    }

    private PatientDiabetesReport getFakePatientDiabetesReport() {
        PatientDiabetesReport patientDiabetesReport = new PatientDiabetesReport();
        Patient patient = new Patient();
        patient.setId(1);
        patient.setFamily("FamilyTest");
        patient.setGiven("GivenTest");
        patient.setDob(LocalDate.of(2000, 1, 1));
        patient.setSex("F");
        patient.setAddress("AddressTest");
        patient.setPhone("PhoneTest");
        patientDiabetesReport.setPatient(patient);
        patientDiabetesReport.setPatientDiabetesRating(PatientDiabetesRating.DANGER.toString());
        return patientDiabetesReport;
    }
}
