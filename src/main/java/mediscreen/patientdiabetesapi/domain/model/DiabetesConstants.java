package mediscreen.patientdiabetesapi.domain.model;

import java.util.List;

public class DiabetesConstants {
    public static final List<String> TRIGGERING_FACTORS = List.of(
            "Hémoglobine A1C",
            "Hemoglobin A1C",
            "Microalbumine",
            "Microalbumin",
            "Taille",
            "Height",
            "Poids",
            "Weight",
            "Fumeur",
            "Smoker",
            "Anormal",
            "Abnormal",
            "Cholestérol",
            "Cholesterol",
            "Vertige",
            "Dizziness",
            "Rechute",
            "Relapse",
            "Réaction",
            "Reaction",
            "Anticorps",
            "Antibodies"
    );
}
