package mediscreen.patientdiabetesapi.domain.model;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum PatientDiabetesRating {
    NONE("None"),
    BORDERLINE("Borderline"),
    DANGER("In Danger"),
    EARLY("Early Onset");

    private final String label;

    @Override
    public String toString() {
        return label;
    }
}
