package mediscreen.patientdiabetesapi.domain.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientNote {

    private String id;
    private Integer patientId;
    private String patientNote;

}