package mediscreen.patientdiabetesapi.domain.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class PatientDiabetesReport {
    private Patient patient;
    private String patientDiabetesRating;
}
