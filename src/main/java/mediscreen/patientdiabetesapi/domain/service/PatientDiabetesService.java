package mediscreen.patientdiabetesapi.domain.service;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.patientdiabetesapi.dal.client.PatientApiClient;
import mediscreen.patientdiabetesapi.dal.client.PatientNoteApiClient;
import mediscreen.patientdiabetesapi.domain.model.DiabetesConstants;
import mediscreen.patientdiabetesapi.domain.model.Patient;
import mediscreen.patientdiabetesapi.domain.model.PatientDiabetesRating;
import mediscreen.patientdiabetesapi.domain.model.PatientDiabetesReport;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

import java.time.Period;
import java.util.stream.Collectors;

@Slf4j
@AllArgsConstructor
@Service
public class PatientDiabetesService {

    private final PatientApiClient patientApiClient;
    private final PatientNoteApiClient patientNoteApiClient;
    private final DateHelper dateHelper;

    public PatientDiabetesReport getPatientDiabetesReportByPatientId(Integer patientId) {
        PatientDiabetesReport patientDiabetesReport = new PatientDiabetesReport();

        Patient patient = patientApiClient.getPatientById(patientId);
        patientDiabetesReport.setPatient(patient);

        patientDiabetesReport.setPatientDiabetesRating(getPatientDiabetesRating(patient).toString());

        return patientDiabetesReport;
    }

    private PatientDiabetesRating getPatientDiabetesRating(Patient patient) {
        int patientAge = Period.between(patient.getDob(), dateHelper.now()).getYears();
        log.debug("patientAge:{}", patientAge);

        String patientSex = patient.getSex();
        log.debug("patientSex:{}", patientSex);

        String flatPatientNotesContents = getFlatPatientNotesContents(patient.getId());
        int patientTriggeringFactorCount = getPatientTriggeringFactorCount(flatPatientNotesContents);
        log.debug("patientTriggeringFactorCount:{}", patientTriggeringFactorCount);
        log.debug("flatPatientNotesContents:{}", flatPatientNotesContents);

        return evaluatePatientDiabetesRating(patientTriggeringFactorCount, patientAge, patientSex);
    }

    private String getFlatPatientNotesContents(Integer patientId) {
        return patientNoteApiClient.getPatientNotesByPatientId(patientId)
                .stream()
                .map(patientNote -> patientNote.getPatientNote().replace("\r\n", " ; "))
                .collect(Collectors.joining(" ; "));
    }

    private int getPatientTriggeringFactorCount(String flatPatientNotesContents) {
        return DiabetesConstants.TRIGGERING_FACTORS.stream()
                .map(triggeringFactor -> StringUtils.countMatches(flatPatientNotesContents.toLowerCase(), triggeringFactor.toLowerCase()))
                .reduce(0, Integer::sum);
    }

    private PatientDiabetesRating evaluatePatientDiabetesRating(int patientTriggeringFactorCount, int patientAge, String patientSex) {
        if ((patientAge <= 30 && "m".equalsIgnoreCase(patientSex) && patientTriggeringFactorCount >= 5) ||
                (patientAge <= 30 && "f".equalsIgnoreCase(patientSex) && patientTriggeringFactorCount >= 7) ||
                (patientAge > 30 && patientTriggeringFactorCount >= 8)) {
            return PatientDiabetesRating.EARLY;
        }

        if ((patientAge <= 30 && "m".equalsIgnoreCase(patientSex) && patientTriggeringFactorCount >= 3) ||
                (patientAge <= 30 && "f".equalsIgnoreCase(patientSex) && patientTriggeringFactorCount >= 4) ||
                (patientAge > 30 && patientTriggeringFactorCount >= 6)) {
            return PatientDiabetesRating.DANGER;
        }

        if (patientAge > 30 && patientTriggeringFactorCount >= 2) {
            return PatientDiabetesRating.BORDERLINE;
        }

        return PatientDiabetesRating.NONE;
    }

}
