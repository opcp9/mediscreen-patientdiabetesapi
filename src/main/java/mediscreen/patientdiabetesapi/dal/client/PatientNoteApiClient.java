package mediscreen.patientdiabetesapi.dal.client;

import mediscreen.patientdiabetesapi.domain.model.PatientNote;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@FeignClient(value = "PatientNotesApiClient", url = "${clients.patientnoteapiclienturl}", path = "/patientnote")
public interface PatientNoteApiClient {
    @GetMapping("/patient/{patientid}")
    List<PatientNote> getPatientNotesByPatientId(@PathVariable("patientid") Integer patientId);
}
