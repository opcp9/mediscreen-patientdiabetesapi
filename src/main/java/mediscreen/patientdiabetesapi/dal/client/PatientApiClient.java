package mediscreen.patientdiabetesapi.dal.client;

import mediscreen.patientdiabetesapi.domain.model.Patient;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(value = "PatientApiClient", url = "${clients.patientapiclienturl}", path = "/patient")
public interface PatientApiClient {

    @GetMapping("/{id}")
    Patient getPatientById(@PathVariable("id") Integer id);
}
