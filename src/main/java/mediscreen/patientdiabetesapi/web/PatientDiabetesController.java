package mediscreen.patientdiabetesapi.web;

import feign.FeignException;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mediscreen.patientdiabetesapi.domain.service.PatientDiabetesService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@AllArgsConstructor
@RestController
@RequestMapping("/patientdiabetes")
public class PatientDiabetesController {

    private PatientDiabetesService patientDiabetesService;

    @GetMapping()
    public ResponseEntity<?> getPatientDiabetesReportByPatientId(@RequestParam Integer patientId) {
        log.debug("request for getting patientDiabetesReport for patientId={}", patientId);

        try {
            return ResponseEntity.status(HttpStatus.OK).body(patientDiabetesService.getPatientDiabetesReportByPatientId(patientId));
        } catch (FeignException e) {
            String logAndBodyMessage = "error while getting patientDiabetesReport because of missing patient with id=" + patientId;
            log.error(logAndBodyMessage);
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(logAndBodyMessage);
        }
    }

}
